const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

//------------------------------------------------------------------------------------

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}


//------------------------------------------------------------------------------------

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
				if(result == null){
					return false
				}
		else{
			//compareSynce is bcrypt function to compare a hashed password to unhashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)};
				}
				else{
					
					return false;
				}

		}
	})
	
}//-------------------------------------------------------------------------------------


module.exports.getUser = (req, res) => {
   const userId = auth.decode(req.headers.authorization).id;

    console.log(userId);
    console.log(userId == 'null')

    if (userId == 'null') {
        return res.status(400).send(false);
    }

    
    return User.findById(userId).then(result => {
        result.password = '';
        
        return res.send(result);
    })
}

//..........................................................................................


module.exports.checkEmail = (req, res) => {
	const email = req.body.email
	console.log(email)

	req.body.email

	User.find({email:email}).then(result => {
		console.log(result)
		if(result.length == 0){
			return res.send(false)
		} 

		return res.send(true)
	})
}




//archive product---------------------------------------------------------------------------

module.exports.archiveProduct = (productId) => {
    return Product.findByIdAndUpdate(productId, {
        isActive: false
    })
    .then((archiveProduct, error) => {
        if(error){
            return false
        } 

        return {
            message: "Product archived successfully!"
        }
    })
};



